const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile No. is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	type: {
		type: String,
		required: [true, "Type of user is required"]
	}

})

module.exports = mongoose.model("User", userSchema)






