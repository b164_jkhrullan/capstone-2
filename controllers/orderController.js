const Order = require("../models/Order");

const bcrypt = require('bcrypt');

module.exports.createOrder = (reqBody) => {

	let newOrder = new Order({
		userId: reqBody.userId,
		products: reqBody.products,
		status: reqBody.status
		
	})

	return newOrder.save().then((order, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.getUserOrders = (userId) => {
	return Order.find({userId: userId}).then( result => {
		return result;
	})
}

module.exports.getAllOrders = () => {
	return Order.find({}).then( result => {
		return result;
	})
}

