const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	sku: {
		type: String,
		required: [true, "sku is required"]
	},
	
	name: {
		type: String,
		required: [true, "Name is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	stocks: {
		type: Number,
		required: [true, "Stocks is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	origin: {
		type: String,
		required: [true, "Origin of product is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	}


})

module.exports = mongoose.model("Product", productSchema)





