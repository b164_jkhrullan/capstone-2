const express = require("express");

const router = express.Router();

const UserController = require("../controllers/UserController");

const auth = require('../auth');

router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});
router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});

router.put("/:userId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization)

	if(user.isAdmin){
		UserController.updateUser(req.params.userId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})






























module.exports = router;