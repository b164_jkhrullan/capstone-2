const Product = require("../models/Product");

module.exports.fetchAllProducts = () => {
	return Product.find({
	}); 

} 

module.exports.fetchById = (id) => {
	return Product.findById(id);
}

module.exports.fetchActiveProducts = () => {
	return Product.find({
		isActive: true
	})
}

module.exports.createProduct = (reqBody) => {

	let newProduct = new Product({
		sku: reqBody.sku,
		name: reqBody.name,
		description: reqBody.description,
		stocks: reqBody.stocks,
		price: reqBody.price,
		origin: reqBody.origin,
		isActive: reqBody.isActive
	})

	return newProduct.save().then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.updateProduct = (productId, reqBody) => {
	let updateProduct = {
		sku: reqBody.sku,
		name: reqBody.name,
		description: reqBody.description,
		stocks: reqBody.stocks,
		price: reqBody.price,
		origin: reqBody.origin,
		isActive: reqBody.isActive
	};
	return Product.findByIdAndUpdate(productId, updateProduct).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive : false
	}
	return Product.findByIdAndUpdate(reqParams, updateActiveField).then((product, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
}







