const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required"]

	},

	products: [
		{
			productId: {
				type: String,
				required: [true, "productId is required"]
			},
			quantity: {
				type: Number,
				required: [true, "quantity is required"]
			}

		}

	],

	status: {
		type: String,
		required: [true, "status is required"]
	}
})




module.exports = mongoose.model("Order", orderSchema)